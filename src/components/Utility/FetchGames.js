import twitch from "@/util/twitch";

import FetchCards from "./FetchCards";
import TwitchInfo from "components/TwitchInfo";

const FetchGames = state =>
  FetchCards(
    {
      component: TwitchInfo.GameInfo,
      fetch: twitch.topGames
    },
    state
  );

export default FetchGames;
