import twitch from "@/util/twitch";

import FetchCards from "./FetchCards";
import TwitchInfo from "components/TwitchInfo";

const FetchStreams = (id = undefined) => state =>
  FetchCards(
    {
      component: TwitchInfo.StreamInfo,
      fetch: () => twitch.streams(id)
    },
    state
  );

export default FetchStreams;
