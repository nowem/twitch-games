import { compose, lifecycle, withState } from "recompose";

import Spinner from "components/LoadingSpinner";
import LoadingHandler from "./LoadingHandler";

const InitialCardsState = withState("cards", "setCards", []);
const LoadingState = withState("loading", "setLoading", true);

const route = entry =>
  entry.id ? `/games/${entry.id}` : `https://www.twitch.tv/${entry.user}`;

const FetchCards = ({ component: InfoComponent, fetch: fetchFunc }) =>
  lifecycle({
    componentDidMount() {
      const { setLoading, setCards, setHeader } = this.props;
      setLoading(true);

      fetchFunc().then(response => {
        const cards = response.data.map(entry => ({
          to: route(entry),
          content: InfoComponent(entry),
          image: entry.image
        }));

        if (setHeader) {
          setHeader(response.header);
        }

        setTimeout(() => {
          setCards(cards);
          setLoading(false);
        }, 400);
      });
    }
  });

export default (fetchOptions, InitialState = x => x) =>
  compose(
    InitialState,
    InitialCardsState,
    LoadingState,
    FetchCards(fetchOptions),
    LoadingHandler(Spinner)
  );
