import { withState } from "recompose";

const WithHeader = withState("header", "setHeader", "");

export default WithHeader;
