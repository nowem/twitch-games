import { branch, renderComponent } from "recompose";

const loadingPropTester = ({ loading }) => loading;

const LoadingHandler = Loader => branch(loadingPropTester, renderComponent(Loader));

export default LoadingHandler;
