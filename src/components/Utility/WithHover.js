import { withState } from "recompose";

const withHover = (initial = "") => withState("hoverClass", "setHover", initial);

export default withHover;
