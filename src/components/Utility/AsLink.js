import React from "react";
import { Link } from "react-router-dom";

const styles = {
  textDecoration: "none",
  color: "#e3e3aa"
};

const WithLink = (Wrapped, LinkComp = Link) => ({ to, ...props }) => (
  <LinkComp style={styles} to={to}>
    <Wrapped {...props} />
  </LinkComp>
);

const ExternalLink = ({ to, children }) => (
  <a style={styles} className="link" href={to}>
    {children}
  </a>
);

const AsLink = (Wrapped, external = false) => {
  const LinkComp = external ? ExternalLink : Link;

  return WithLink(Wrapped, LinkComp);
};

export default AsLink;
