import React from "react";
import Card from "components/Card";
import "./CardGrid.css";

const CardGrid = ({ cards, component: Component, header }) => (
  <div className="card-grid">
    <div className="card-grid__header">{header ? header : "no"}</div>
    <div className="card-grid__content">
      {cards.map((props, i) =>
        Component ? <Component key={i} {...props} /> : <Card key={i} {...props} />
      )}
    </div>
  </div>
);

export default CardGrid;
