import React from "react";
import { NavLink } from "react-router-dom";
import "./Tab.css";

const Tab = ({ name, path }) => (
  <div className="tab">
    <NavLink exact className="tab__link" activeClassName="tab--active" to={path}>
      <div className="tab__text">{name}</div>
    </NavLink>
  </div>
);

export default Tab;
