import React from "react";
import { Route, Switch } from "react-router-dom";

const Routes = ({ routes }) => (
  <Switch>
    {routes.map(route => (
      <Route
        key={route.path}
        exact={route.exact || false}
        path={route.path}
        component={route.component}
      />
    ))}
  </Switch>
);

export default Routes;
