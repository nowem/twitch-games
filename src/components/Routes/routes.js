import Game from "@/pages/Game";
import TopGames from "@/pages/TopGames";
import Streams from "@/pages/Streams";

const routes = [
  {
    path: "/",
    component: TopGames,
    exact: true
  },
  {
    path: "/games",
    component: TopGames,
    exact: true,
    tab: "Games"
  },
  {
    path: "/games/:id",
    component: Game
  },
  {
    path: "/streams",
    component: Streams,
    exact: true,
    tab: "Streams"
  }
];

export default routes;
