import React from "react";
import TabPanel from "components/TabPanel";
import "./Header.css";

import routes from "components/Routes/routes";

const tabs = routes
  .filter(route => !!route.tab)
  .map(route => ({
    name: route.tab,
    path: route.path
  }));

const Header = () => (
  <div className="header">
    <TabPanel tabs={tabs} />
  </div>
);

export default Header;
