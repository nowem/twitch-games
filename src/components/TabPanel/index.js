import React from "react";
import Tab from "components/Tab";
import "./TabPanel.css";

const TabPanel = ({ tabs }) => (
  <nav className="tab-panel">
    {tabs.map(tab => (
      <Tab key={tab.name} {...tab} />
    ))}
  </nav>
);

export default TabPanel;
