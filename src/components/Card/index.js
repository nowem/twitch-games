import React from "react";

import "./Card.css";
import placeholder from "assets/image-placeholder.png";

const Card = ({ content, image = placeholder, hoverClass, setHover }) => {
  return (
    <div className="card__container">
      <div
        className="card__media"
        onMouseEnter={() => setHover("text-background-light card__content--hover")}
        onMouseLeave={() => setHover("text-background-black")}
      >
        <img className="card__image" src={image} alt="" />
      </div>

      <div className={`card__content ${hoverClass}`}>{content}</div>
    </div>
  );
};
export default Card;
