import GameInfo from "./GameInfo";
import StreamInfo from "./StreamInfo";

export default {
  GameInfo,
  StreamInfo
};
