import React from "react";
import "./GameInfo.css";

const GameInfo = ({ name }) => (
  <div className="game-info">
    <div className="text-background-fill">{name}</div>
  </div>
);

export default GameInfo;
