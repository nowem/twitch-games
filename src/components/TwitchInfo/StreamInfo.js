import React from "react";
import "./StreamInfo.css";

const LiveIcon = ({ status }) => {
  const iconClass = status === "live" ? "online" : "offline";

  return (
    <svg className={`live-icon ${iconClass}`}>
      <circle cx="15" cy="12" r="7" />
    </svg>
  );
};

const StreamInfo = ({ title, user, viewers, status }) => {
  return (
    <div className="info">
      <div className="info__user">
        <div>
          <div className="text-background-fill">
            {user} <LiveIcon status={status} />
          </div>
        </div>

        <div className="info__viewers">
          <div className="text-background-fill">{viewers}</div>
        </div>
      </div>

      <div className="info__title">
        <div className="text-background-fill">{title}</div>
      </div>
    </div>
  );
};

export default StreamInfo;
