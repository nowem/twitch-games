import React from "react";

import Page from "@/pages/Page";
import FetchStreams from "components/Utility/FetchStreams";

import WithHeader from "components/Utility/WithHeader";

const Game = ({ match }) => {
  const id = match.params.id;
  const GamePage = Page(FetchStreams(id), WithHeader, true);

  return <GamePage style={{ lineHeight: "2.rem" }} />;
};

export default Game;
