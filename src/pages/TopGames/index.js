import React from "react";

import Page from "@/pages/Page";
import FetchGames from "components/Utility/FetchGames";

import WithHeader from "components/Utility/WithHeader";

const TopGames = () => {
  const TopGamesPage = Page(FetchGames, WithHeader);
  const header = "Most Popular Games";

  return <TopGamesPage header={header} />;
};

export default TopGames;
