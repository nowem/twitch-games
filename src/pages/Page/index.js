import React from "react";
import { withProps, compose } from "recompose";
import "./Page.css";

import Card from "components/Card";
import CardGrid from "components/CardGrid";

import WithHover from "components/Utility/WithHover";
import AsLink from "components/Utility/AsLink";

const GamesCard = (FetchComponent, external = false) => {
  const HoverCard = WithHover("text-background-black")(AsLink(Card, external));
  const withComponent = withProps({ component: HoverCard });

  return compose(
    FetchComponent,
    withComponent
  );
};

const CardGridPage = (FetchComponent, state, external = false) => ({ header }) => {
  const WithFetch = GamesCard(FetchComponent(state), external);
  const FetchedCardGrid = WithFetch(CardGrid);

  return (
    <div className="page">
      <FetchedCardGrid />
    </div>
  );
};

export default CardGridPage;
