import React from "react";

import Page from "@/pages/Page";
import FetchStreams from "components/Utility/FetchStreams";
import WithHeader from "components/Utility/WithHeader";

const Streams = () => {
  const StreamsPage = Page(FetchStreams(), WithHeader, true);
  const header = "Most Popular Streams";

  return <StreamsPage header={header} />;
};

export default Streams;
