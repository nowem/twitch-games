import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "./App.css";

import Header from "components/Header";
import Routes from "components/Routes";
import routes from "components/Routes/routes";

const App = () => {
  return (
    <div className="App">
      <Router>
        <div className="App__header">
          <Header />
        </div>
        <div className="App__body">
          <Routes routes={routes} />
        </div>
      </Router>
    </div>
  );
};

export default App;
