import React from "react";
import axios from "axios";
import config from "config/twitch";

const endpoint = axios.create({
  baseURL: config.url,
  headers: config.headers
});

const transformImage = imageUrl =>
  imageUrl.replace("{width}", "400").replace("{height}", "450");

const transformResponse = response => ({
  title: response.title,
  user: response.user_name,
  viewers: response.viewer_count,
  status: response.type,
  gameid: response.game_id,
  image: transformImage(response.thumbnail_url)
});

const fetchGameName = id =>
  new Promise((resolve, reject) => {
    if (id)
      endpoint
        .get(`/games?id=${id}`)
        .then(res => res.data)
        .then(res => resolve(res.data[0].name));
    else resolve(undefined);
  });

export default {
  streams: (game = undefined) => {
    const query = game ? `?game_id=${game}` : "";
    const gamename = fetchGameName(game);
    const streams = endpoint.get(`/streams${query}`).then(res => res.data);

    return Promise.all([gamename, streams])
      .then(([gamename, streams]) => {
        const topStreams = "Most Popular Streams";
        const gameStreams = game => (
          <span>
            {topStreams} for <br />
            <span style={{ fontWeight: "bold" }}>{game}</span>
          </span>
        );
        const header = gamename ? gameStreams(gamename) : topStreams;

        return [header, streams];
      })
      .then(([header, streams]) => ({
        header,
        data: streams.data.map(transformResponse),
        cursor: streams.pagination.cursor
      }))
      .catch(err => {
        console.error(err.message);
        throw err;
      });
  },
  topGames: (cursor = undefined) => {
    const params = { after: cursor };
    return endpoint
      .get("/games/top", params)
      .then(res => res.data)
      .then(res => ({
        header: "Most Popular Games",
        cursor: res.pagination.cursor,
        data: res.data.map(({ box_art_url, name, id }) => ({
          image: transformImage(box_art_url),
          name,
          id
        }))
      }))
      .catch(err => {
        console.error(err.message);
        throw err;
      });
  }
};
