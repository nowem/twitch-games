import twitch from "@/util/twitch";

describe("Twitch API module", () => {
  it("fetches stream information correctly", async () => {
    const streams = await twitch.streams();

    expect(streams.data).toHaveLength(20);
  });

  it("fetches stream information correctly", async () => {
    const games = await twitch.topGames();

    expect(games.data).not.toBe([]);
  });
});
