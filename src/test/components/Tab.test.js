import { shallow } from "enzyme";
import Tab from "components/Tab";

const Shallow = tab => shallow(Tab(tab));
const tab = { name: "tab", path: "#" };

describe("<Tab />", () => {
  it("renders without errors", () => {
    Shallow(tab);
  });

  it("renders correct link", () => {
    const wrapper = Shallow(tab);
    const link = wrapper.find(".tab__link");

    expect(link.prop("to")).toEqual(tab.path);
  });

  it("renders correct tab text", () => {
    const wrapper = Shallow(tab);
    const tabText = wrapper.find(".tab__text");

    expect(tabText.text()).toEqual(tab.name);
  });
});
