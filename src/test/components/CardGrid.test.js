import React from "react";

import { shallow } from "enzyme";

import CardGrid from "components/CardGrid";

describe("CardGrid component", () => {
  const numOfChildren = 20;
  const props = { content: "Hello World", to: "#", component: jest.fn() };
  const cards = [];

  const ShallowMount = prop => shallow(CardGrid(prop));

  for (let i = 0; i < numOfChildren; i++) {
    cards.push(props);
  }

  it("renders without crashing", () => {
    ShallowMount({ cards });
  });

  it("renders all children", () => {
    const wrapper = ShallowMount({ cards });

    expect(wrapper.find(".card-grid__content").children()).toHaveLength(numOfChildren);
  });
});
