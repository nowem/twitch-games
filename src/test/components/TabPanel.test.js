import { shallow } from "enzyme";
import TabPanel from "components/TabPanel";

describe("<TabPanel />", () => {
  const Shallow = tabs => shallow(TabPanel(tabs));
  const tabs = [{ name: "tab", route: "#" }, { name: "tab", route: "#" }];

  it("renders without errors", () => {
    Shallow({ tabs });
  });

  it("renders correct number of tabs", () => {
    const wrapper = Shallow({ tabs });

    expect(wrapper.children()).toHaveLength(tabs.length);
  });
});
