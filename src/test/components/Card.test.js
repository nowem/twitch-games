import React from "react";

import { shallow } from "enzyme";

import Card from "components/Card";

describe("Card component", () => {
  const props = {
    content: "Hello World",
    hoverClass: "card--hover",
    setHover: jest.fn()
  };

  it("renders without crashing", () => {
    shallow(<Card {...props} />);
  });

  it("renders default image placeholder", () => {
    const wrapper = shallow(<Card {...props} />);
    expect(wrapper.find(".card__image").prop("src")).toEqual("image-placeholder.png");
  });

  it("renders image prop", () => {
    const image =
      "https://res.cloudinary.com/teepublic/image/private/s--imtLQwII--/t_Preview/b_rgb:ffffff,c_limit,f_jpg,h_630,q_90,w_630/v1537945289/production/designs/3214340_0.jpg";
    const wrapper = shallow(<Card image={image} {...props} />);
    expect(wrapper.find(".card__image").prop("src")).toEqual(image);
  });

  it("renders header correctly", () => {
    const wrapper = shallow(<Card {...props} />);

    expect(wrapper.find(".card__content").text()).toEqual(props.content);
  });
});
