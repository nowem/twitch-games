import { shallow } from "enzyme";
import Header from "components/Header";

describe("<Header />", () => {
  const Shallow = tabs => shallow(Header(tabs));
  const tabs = [{ name: "tab", route: "#" }, { name: "tab", route: "#" }];

  it("renders without errors", () => {
    Shallow(tabs);
  });
});
