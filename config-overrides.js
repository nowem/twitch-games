const path = require("path");

module.exports = (config, env) => {
  if (!config.resolve) {
    config.resolve = { alias: {} };
  }

  if (!config.resolve.alias) {
    config.resolve.alias = {};
  }

  const alias = config.resolve.alias;

  config.resolve.alias = {
    components: path.resolve(__dirname, "src/components"),
    assets: path.resolve(__dirname, "src/assets"),
    config: path.resolve(__dirname, "src/config"),
    "@": path.resolve(__dirname, "src"),
    ...alias
  };

  return config;
};
